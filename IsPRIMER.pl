#! /usr/bin/perl

###Author: Devon Radford
###Date created: September 12, 2019
###Date modified: March 26, 2020
### Step 1: quantify matches between a set of primers and a reference sequence database

## HEAD
use File::Basename;
use File::Spec;
use Cwd;
my $isprime_dir=Cwd::abs_path($0);
my $lib_dir=File::Basename::dirname($isprime_dir);
print "\nisPRIME scripts are in $lib_dir! \n\n";
#my $lib_dir;
#my $libs="/Users/chenw/Documents/ubuntu_64bit_shared/Bitbucket_repos/isprime";
#use lib "/Users/chenw/Documents/ubuntu_64bit_shared/Bitbucket_repos/isprime";
#use lib abs_path($lib_dir);
#print bs_path($lib_dir);
use lib dirname (__FILE__);
use Primer;
use Fcntl qw(:flock);
use List::Util qw(sum);

##my $debug=1;

## Input Handling
my $seqFilename=undef; ## filename for reference sequence database in fasta format
my $primerFilename=undef; ## filename for primer list
my $outname=undef; ## filename for outputfile to produce
my $forks=1; ##number of forks for parallel processing

my $testFlag=0; ## flag to mark if the program should run in testing mode or not
my $validationFlag=0; ## flag to mark if the input should be validated
my $pm=undef; ##memory holder for parallel form manager if the program needs to run in parallel

##file locking and unlocking subroutines, need to establish ahead of use
sub flush {
   my $h = select($_[0]); my $af=$|; $|=1; $|=$af; select($h);
}

sub lock {
	my ($fh) = @_;
	flock($fh, LOCK_EX) or die "Cannot lock file - $!\n";
        # and, in case we're running on a very old UNIX
        # variant without the modern O_APPEND semantics...
	seek($fh, 0, SEEK_END) or die "Cannot seek - $!\n";
}

sub unlock {
	my ($fh) = @_;
	flock($fh, LOCK_UN) or die "Cannot unlock file - $!\n";
}


my @time = localtime();

# generates YYYYMMDDhhmmss
my $timestring = sprintf("%04d%02d%02d%02d%02d%02d",
                        $time[5]+1900, $time[4]+1, $time[3],
                        $time[2],      $time[1],   $time[0]);

my $lastFlag="";
foreach $arg(@ARGV){
	if($lastFlag eq "--o"){
		if($testFlag){
			$outname="$arg".".$timestring".".tab";
		}else{
			$outname="$arg".".1";
		}
	}elsif($lastFlag eq "--r"){
		$seqFilename="$arg";
	}elsif($lastFlag eq "--p"){
		$primerFilename="$arg";
	}elsif($lastFlag eq "--v"){
		$validationFlag=$arg;
	}elsif($lastFlag eq "--f"){
		$forks=$arg;
	}elsif($lastFlag eq "--h"){ ##help
		print "isPRIME\nAuthors: Devon Radford, Wen Chen\n";
		print "Purpose: Software to quantify matches between a set of primers and a taxonomically annotated reference sequence database\n";
		print "Command options:\n";
		print "\t--h\thelp\n";
		print "\t--f\tnumber of forks to use during processing\n";
		print "\t--o\tdefine the filename to which output should be directed\t\t\t(required for all steps)\n";
		print "\t--p\tdefine a file with the list of primers\t\t\t\t\t(required for step 1)\n";
		print "\t--r\tdefine a file with the set of reference sequences to assess\t\t\t(required for step 1)\n";
		print "\t--v\tturn on(1) or off(0) input validation. Default off\n";
		exit;
	}
	$lastFlag="$arg";
}
if($lastFlag eq "--h"){ ##help
	print "isPRIME\nAuthors: Devon Radford, Wen Chen\n";
	print "Purpose: Software to quantify matches between a set of primers and a taxonomically annotated reference sequence database\n";
	print "Command options:\n";
	print "\t--h\thelp\n";
	print "\t--f\tnumber of forks to use during processing\n";
	print "\t--o\tdefine the filename to which output should be directed\t\t\t(required for all steps)\n";
	print "\t--p\tdefine a file with the list of primers\t\t\t\t\t(required for step 1)\n";
	print "\t--r\tdefine a file with the set of reference sequences to assess\t\t\t(required for step 1)\n";
	print "\t--v\tturn on(1) or off(0) input validation. Default on\n";
	exit;
}

if($fork > 1){
	require Parallel::ForkManager;
}

##BODY
##open output file
##open($OUT,"> $outname") || die("Failed to open step 1 output file, $outname\n$!");
my %outHash={};

##process annotated list of primers
unless($primerFilename){
	print "primer file name must be defined on command line\n";
	exit;
}unless(-e $primerFilename){
	print "$primerFilename not found\n";
	exit;
}unless(open(IN,$primerFilename)){
	print "$primerFilename failed to open\n";
	exit;
}
$primCount=0;
my @primerTable=();
foreach $line(<IN>){
    chomp $line;
    if($line=~/^([^\t\n]+)\t([ATCGYRSWKMBVDHNI]+)$/){
    	($primName,$thisPrimer)=($1,$2);
    	push(@primerTable,Primer->new($thisPrimer, $primName));
		##Generate Primer Specific Filehandle Hash
		open(my $OUT,">", "$outname".".$primName") || die("Failed to open step 1 output file, $outname".".$primName\n$!");
		print $OUT "# oligo ".$primCount.", $primName:\t$thisPrimer\tcomplement:\t".$primerTable[$primCount]->getCompSeq()."\n";
    	##&flush($OUT);
    	$outHash{$primName}=$OUT;
    	print "$OUT\t# oligo ".$primCount.", $primName:\t$thisPrimer\tcomplement:\t".$primerTable[$primCount]->getCompSeq()."\n" if($debug);
    	$primCount++;
    }else{
        print "Step 1, line 51: ".$line;
    }
}
close IN;

my $gi="";
my $acc="";
my $seq="";
my $primerMatchStr="";

##process database of reference sequences
unless($seqFilename){
	print "sequence file name must be defined on command line\n";
	##close $OUT;
	&closeFhHash(\%outHash);
	exit;
}unless(-e $seqFilename){
	print "$seqFilename not found\n";
	##close $OUT;
	&closeFhHash(\%outHash);
	exit;
}unless(open(IN,$seqFilename)){
	print "$seqFilename failed to open\n";
	##close $OUT;
	&closeFhHash(\%outHash);
	exit;
}

foreach $primer(@primerTable){
	$primName=$primer->getName();
	##&lock($outHash{$primName});
	print {$outHash{$primName}} "# reference sequence database:\t$seqFilename\nGI\tACC\tname\tseq_length\t".$primName."_mismatchNum\t".$primName."_strand\t".$primName."_match\n";
	##&flush($outHash{$primName});
	##&unlock($outHash{$primName});
	print "# reference sequence database:\t$seqFilename\nGI\tACC\tname\tseq_length\t".$primName."_mismatchNum\t".$primName."_strand\t".$primName."_match\n" if($debug);
	close($outHash{$primName});
}
##print $OUT "\n";
##print "outHash: ".\%outHash."\n" if($debug);
##&closeFhHash(\%outHash);

##instantiate fork manager
if($forks>1){
	$pm = Parallel::ForkManager->new($forks);
}

##process database
my $pid=0;
my $linesProcessed=0;
foreach $line(<IN>){
	chomp $line;
	#remove white space at the begininig and end of sequences
	$line=~ s/[\t ]+//g;
	if ($line=~ /^>/) {
		$linesProcessed++;
		if ($validationFlag==0 && $line=~ /^>(.*)\|(.*)/) { ##accept any | delineated input header
			$line=~ s/^>//g;
			my @lns = split /\|/, $line;
			#print $lns[0], "\t", $lns[1], "\n" if($testFlag);
			($nextName,$nextAcc)=($lns[0],$lns[1]);
			#print $lns[0], "\t", $lns[1], "\n";
			&processSeq($gi,$acc,$name,$seq,$outname,\@$primerTable,$forks,$pm);
			$gi="";
			$acc="$nextAcc";
			$name="$nextName";
			$seq="";
		} elsif ($line=~/^>([^\t\n\|]+)\|([^\t\n\|]+)/){ ## validated relaxed UNITE general release format
			($nextName,$nextAcc)=($1,$2);
			&processSeq($gi,$acc,$name,$seq,$outname,\@$primerTable,$forks,$pm);
			$gi="";
			$acc="$nextAcc";
			$name="$nextName";
			$seq="";
		} else {
			print "Step 1: unrecognized seqID format: $line";
    	}
	} elsif ($line=~/^([A-Ya-y]+)$/) {
		$seq.=uc $1;
		$seq=~ s/[\t ]+//g;
	} elsif($line=~/^$/){
	} else {
    	print "\nStep 1: unrecognized format: $line";
    }

	if($linesProcessed % 10000 == 0){
		print "\nProcessed $linesProcessed sequences\n";
	}
}
&processSeq($gi,$acc,$name,$seq,$outname,\@$primerTable,$forks,$pm);
close IN;
##close($OUT);
print "Processed $linesProcessed sequences\n";
print "Output is saved to $outname\n\n";

my @endtime = localtime();

# generates YYYYMMDDhhmmss
my $endtimestring = sprintf("%04d%02d%02d%02d%02d%02d",
                        $endtime[5]+1900, $endtime[4]+1, $endtime[3],
                        $endtime[2],      $endtime[1],   $endtime[0]);
print("Elapsed time to end of parent: ".($endtimestring - $timestring)." seconds\n") if($debug);
$pm->wait_all_children if($forks>1);
&closeFhHash(\%outHash);

@endtime = localtime();
# generates YYYYMMDDhhmmss
$endtimestring = sprintf("%04d%02d%02d%02d%02d%02d",
                        $endtime[5]+1900, $endtime[4]+1, $endtime[3],
                        $endtime[2],      $endtime[1],   $endtime[0]);
print("Elapsed time: ".($endtimestring - $timestring)." seconds\n");
exit;

##SUBROUTINES
sub closeFhHash{
	my $fhHash=shift;
	print "hash to close: ".$fhHash."\n" if($debug);
	foreach $key (keys %{$fhHash}){
		print "closing $key ".$fhHash{$key}."\n" if($debug);
		close($fhHash{$key});
	}
	print "hash of output files closed\n" if($debug);
}

sub findMatch{
	my ($primer,$compPrimer,$seq)=@_;
	if($primer eq "" || $compPrimer eq ""){
		print "Error: missing required sequence\nf:$primer\ncf:$compPrimer\n";
		exit;
	}
	my ($strand,$amplicon,$match)=("0","","");
	if($seq=~/[ACTGN]{5}($primer[ACTGN]{5})/i){
		($amplicon,$match)=("$1"."...","$1");
		$strand="D";
	}elsif($seq=~/($primer)/i){
		($amplicon,$match)=("$1"."...","$1");
		$strand="DA";
	}elsif($seq=~/([ACTGN]{5}$compPrimer)[ACTGN]{5}/i){
		($amplicon,$match)=("..."."$1","$1");
		$strand="L";
	}elsif($seq=~/($compPrimer)/i){
		($amplicon,$match)=("..."."$1","$1");
		$strand="LA";
	}
	return ($strand,$amplicon,$match);
}

sub findQuarterMatches{
	my ($thisPrimer,$seq)=@_;
	my ($firstFlag,$secondFlag,$thirdFlag,$fourthFlag)=(0,0,0,0);
	($first,$second,$third,$fourth)=@{$thisPrimer->getSeqQuarters()};
	($cfourth,$cthird,$csecond,$cfirst)=@{$thisPrimer->getCompSeqQuarters()};
	my $length=$thisPrimer->getLength();
	$quarterLength=int($length/4);
	$halfLength=$quarterLength*2;
	$threeQuarterLength=$quarterLength*3;
	if($seq=~/$first$second[ATCGNI]{$halfLength}/ || $seq=~/[ATCGNI]{$halfLength}$csecond$cfirst/){
		($firstFlag,$secondFlag)=(1,1);
		if($seq=~/$first$second$third[ATCGNI]{$quarterLength}/ || $seq=~/[ATCGNI]{$quarterLength}$cthird$csecond$cfirst/){
			$thirdFlag=1;
		}elsif($seq=~/$first$second[ATCGNI]{$quarterLength}$fourth/ || $seq=~/$cfourth[ATCGNI]{$quarterLength}$csecond$cfirst/){
			$fourthFlag=1;
		}
	}elsif($seq=~/[ATCGNI]{$halfLength}$third[ATCGNI]{$quarterLength}/ || $seq=~/[ATCGNI]{$quarterLength}$cthird[ATCGNI]{$halfLength}/){
		$thirdFlag=1;
		if($seq=~/$first[ATCGNI]{$quarterLength}$third[ATCGNI]{$quarterLength}/ || $seq=~/[ATCGNI]{$quarterLength}$cthird[ATCGNI]{$quarterLength}$cfirst/){
			$firstFlag=1;
		}elsif($seq=~/[ATCGNI]{$quarterLength}$second$third[ATCGNI]{$quarterLength}/ || $seq=~/[ATCGNI]{$quarterLength}$cthird$csecond[ATCGNI]{$quarterLength}/){
			$secondFlag=1;
		}
		if($seq=~/[ATCGNI]{$halfLength}$third$fourth/ || $seq=~/$cfourth$cthird[ATCGNI]{$halfLength}/){
			$fourthFlag=1;
		}
	}elsif($seq=~/$first[ATCGNI]{$halfLength}$fourth/ || $seq=~/$cfourth[ATCGNI]{$halfLength}$cfirst/){
		($firstFlag,$fourthFlag)=(1,1);
	}
	if($seq=~/[ATCGNI]{$quarterLength}$second$third$fourth/ || $seq=~/$cfourth$cthird$csecond[ATCGNI]{$quarterLength}/){
		($firstFlag,$secondFlag,$thirdFlag,$fourthFlag)=(0,1,1,1);
	}
	print $thisPrimer->getName()."\tq=$quarterLength\t$firstFlag,$secondFlag,$thirdFlag,$fourthFlag\t$first $cfirst,$second $csecond,$third $cthird,$fourth $cfourth\n" if($debug);
	return ($firstFlag,$secondFlag,$thirdFlag,$fourthFlag);
}

sub processSeq{
	my ($gi,$acc,$name,$seq,$outHash,$primerTable,$forks,$pm)=@_;
	return 0 if($acc eq "");
	##print $OUT "$gi" if($gi ne "");
	##if($acc ne ""){
	##	print $OUT "\t$acc\t$name\t".length($seq);
	##}else{
	##	return 0;
	##}
	for($i=0;$i<=$#primerTable;$i++){
		if($forks>1){
			$pid = $pm->start and next;
		}
		my $primerName=$primerTable[$i]->getName();
		my $forwardPrimer=$primerTable[$i]->getSeq();
	   	my $compFrwdPrimer=$primerTable[$i]->getCompSeq();
	   	my $mismatch=0;
	   	($strand,$amplicon,$frwMatch)=&findMatch($forwardPrimer,$compFrwdPrimer,$seq);
	   	##if less than two 1/4s of the primer sequence exist in the reference there can be no match with less than 2 mismatches
	   	my @quadrentFlags=&findQuarterMatches($primerTable[$i],$seq);
	   	my $quadSum=List::Util->sum(@quadrentFlags);
	   	##print "$acc\t$name\t$primerName\t$quadSum\n" if($debug);
	   	if($quadSum >= 2){
	   		$mismatch=1 if($quadSum < 3);
	   		my $length=$primerTable[$i]->getLength();
			$quarterLength=int($length/4);
	   		#screen possible mismatches
	   		while($strand eq "0" && $mismatch<2){
	   			$mismatch++;
	   			@forwardMismatches=$primerTable[$i]->getMismatchSeq($mismatch);
	   			##print $primerTable[$i]->getName()." mismatch $mismatch\t".$forwardMismatches[0].", first of ".$#forwardMismatches."\n" if($debug);
	   			@compFrwdMismatches=$primerTable[$i]->getCompMismatchSeq($mismatch);
	   			##print $primerTable[$i]->getName()." comp-mismatch $mismatch\t".$compFrwdMismatches[0].", first of ".$#compFrwdMismatches."\n" if($debug);
		   		my @index=();
		   		for($j=0;$j<$mismatch;$j++){
			   		if($quadrentFlags[0]!=1){
				   		push(@index,((0+$j*$length)..($quarterLength+$j*$length)));
					}
					if($quadrentFlags[1]!=1){
				   		push(@index,(($quarterLength+$j*$length+1)..(2*$quarterLength+$j*$length)));
					}
					if($quadrentFlags[2]!=1){
				   		push(@index,((2*$quarterLength+$j*$length+1)..(3*$quarterLength+$j*$length)));
					}
					if($quadrentFlags[3]!=1){
				   		push(@index,((3*$quarterLength+$j*$length+1)..($length+$j*$length)));
					}
				}
				if($debug){
					print "$acc\t$name\t$primerName\t";
					foreach $ind(@index){print "$ind ";}
					print "\n";
				}
		   		##for($h=$startIndex;$h<=$endIndex;$h++){
		   		foreach $h(@index){
					($strand,$amplicon,$frwMatch)=&findMatch($forwardMismatches[$h],$compFrwdMismatches[$h],$seq);
   					if($strand ne "0"){
   						last;
   					}
		   		}
	   		}
	   	}else{
	   		print "no match: $primerName\t$acc\t$name\n" if($debug);
	   	}
	   	unless(open($OUT,">>", "$outname".".$primerName")){
	   		print("Failed to open output file, $outname".".$primerName for $acc\n$!\n");
	   		$pm->finish if($pm);
	   	}
		##my $OUT = $outHash{$primerTable[$i]->getName()};
		##$retStr="$gi\t$acc\t$name\t".length($seq);
		&flush($OUT);
		&lock($OUT);
		##print $OUT "$gi\t$acc\t$name\t".length($seq);
	   	if($strand eq "0"){
	   		print $OUT "$gi\t$acc\t$name\t".length($seq)."\t-\t-\t-\n";
	   		##print "$primerName: $gi\t$acc\t$name\t".length($seq)."\t-\t-\t-\n" if($debug);
	   	}else{
	   		print $OUT "$gi\t$acc\t$name\t".length($seq)."\t$mismatch\t$strand\t$frwMatch\n";
	   		##print "$primerName: $gi\t$acc\t$name\t".length($seq)."\t$mismatch\t$strand\t$frwMatch\n" if($debug);
		}
		&unlock($OUT);
		close($OUT);
		print "$primerName\t$gi\t$acc\t$name\t".length($seq)."\t$mismatch\t$strand\t$frwMatch\n" if($debug);
		$pm->finish if($pm);
		##$pm->finish(0,\$retStr);
	}
	##print $OUT "\n";
	return 0;
}

exit;
