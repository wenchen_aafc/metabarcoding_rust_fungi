#!/usr/bin/env Rscript
# Process AODP output and obtain LCA of taxonomy
# Input is the output of AODP run against a reference DB in UNITE general release FASTA format
# speciesName|Accession1|Accession2|Description|k__kingdom;p__phylum;c__class;o__order;f__family;g__genus;s__species;st__strain

# USAGE
# default
# run_aodp_taxonomy.R -i output.aodp -r ITS

# check operation system;
# this script works only on Linux and OSX systems.
sys <- switch(Sys.info()[['sysname']],
              Windows= {print("I'm a Windows PC.")},
              Linux  = {print("I'm a penguin (Linux).")},
              Darwin = {print("I'm a Mac.")})
if ( sys == "I'm a Windows PC." ) {
  stop("Sorry, I am not functional. This R function works on Linux and OS operating systems")
} else {
  print("OK, this is your lucky day, I am working")
}

# check if required R packages have been installed;
# if not, install the missing packages

package_list <- c("dplyr",
                  "tidyr",
                  "openxlsx",
                  "optparse"
)

#library(phangorn)
#library(gdata)

for (pk in package_list) {
  if(!suppressWarnings(suppressMessages(require(pk, character.only = TRUE,
                                                quietly = TRUE,
                                                warn.conflicts = FALSE)))) {
    install.packages(pk, repos="http://cran.r-project.org")
    suppressWarnings(suppressMessages(library(p, character.only = TRUE,
                                              quietly = TRUE,
                                              warn.conflicts = FALSE)))
  }
}

# options for run_aodp_taxonomy
# reference code: https://cran.r-project.org/web/packages/optparse/vignettes/optparse.html
option_list <- list(
  make_option(c("-v", "--verbose"), action="store_true", default=FALSE,
              help="Print extra output [default]"),
  make_option(c("-i", "--input"), type="character", default=NULL,
              help="input is the output of aodp AODP run against a reference DB in UNITE general release FASTA format [default %default]"),
  make_option(c("-o", "--output_basename"), type="character", default=NULL,
              help="basename of output files [default %default]"),
  make_option(c("-p", "--intraspecific_cutoff"), type="numeric", default=97,
              help="intraspecific variation cutoff [default %default]"),
  make_option(c("-x", "--filter.unidentified.rank"), type="character", default="class",
              help="filter.unidentified.rank [default %default]")

  )
opts <- parse_args(OptionParser(option_list=option_list))

print ("This function generates LCA taxonomy for AODP output")
print ("The RefDB for AODP must be in UNITE General Release FASTA Format, i.e. the sequence IDs should look like: >speciesName|Accession1|Accession2|Description|TaxononomyLineage")
print ("For example: >Puccinia_polysora|GBx|RS2910A|CR|k__Fungi;p__Basidiomycota;c__Pucciniomycetes;o__Pucciniales;f__Pucciniaceae;g__Puccinia;s__Puccinia_polysora;st__Puccinia_polysora_Tripsacum_dactyloides_USA_106612_1962")

# Provide aodp output file as input for this function

if ( !is.na(opts$input) ) {
  if (file.exists(opts$input)) {
    aodp_output_file=opts$input;
    print(paste0("input file exists: ", aodp_output_file));
  } else {
    stop(paste0(opts$input, " doesn't exit!"));
  }
} else {
  stop("must provide the output of aodp AODP run against a reference DB in UNITE general release FASTA format");
}

# output names
if (!is.null(opts$output_basename)){
  opts$output_basename=opts$output_basename;
} else {
  opts$output_basename=paste0(opts$input);
}


########################
# aodp_output_file  "
########################
if (!is.null(aodp_output_file) & file.exists(aodp_output_file)) {
  aodp <- read.table(aodp_output_file, sep="\t",
                     header=FALSE, stringsAsFactors = FALSE)
} else {
  print("must provide the output of aodp AODP run against a reference DB in UNITE general release FASTA format");
  stop(paste(aodp_output_file, " doesn't exist!!!"));
}

dim(aodp)

ranks.aodp=c("kingdom", "phylum", "class",
             "order", "family", "genus", "species", "strain");
aodp_header <- c("ASV", "tax", "percent_identity",
                 "match_length", "ASV_length", "minimum_set_size",
                 "largest_cluster_matched");

dim(aodp) #[1] 1621    7
str(aodp)
names(aodp) <- aodp_header

#aodp[grepl("180941a770d8d17985ebff4bae0f6117", aodp$ASV), ] # should be pseudostriiformis
#aodp[grepl("59473c36be5bc1e38118dd43dfd7a192", aodp$ASV), ] # use "its_rep.UNITE.SH_CR.16.aodp" as DB, all becomes striiformis


# because a single target sequence may match with multiple source seq in RefDB
dim(aodp) # 3836, 7
length(unique(aodp$ASV)) # 485, only Pucciniales from sintax (confidence = 0.8)
# column $ percent_identity is characters, NOT number : chr  "0.0%" "0.0%" "0.0%" "0.0%" ...
# convert aodp$percent_identity to numeric,
# for those e.g. <75% etc, convert to NA
# aodp$percent_identity <- as.numeric(sub("%", "", aodp$percent_identity))
# Warning message:
# NAs introduced by coercion- forced to
# after converting to numeric: min(aodp$percent_identity[aodp$tax !="-"]) # 80.4
# after converting to numeric: max(aodp$percent_identity[aodp$tax !="-"]) # 100


########################
# remove no taxonomy ASVs and convert percent_identity to numeric
########################

aodp.fil <- aodp %>%
  filter(tax != "-") %>%
  mutate(percent_identity=as.numeric(sub("%","", percent_identity))) %>%
  group_by(ASV) %>% # for each ASV choose only the best percent_identity
  arrange(dplyr::desc(percent_identity)) %>%
  # keep only match source with best values
  filter(percent_identity == max(percent_identity)) %>%
  # filter by 3rd quatile interspecific variation: 0.0152 =1-0.0152
  # filter percent_identity >=1-0.0152
  filter(percent_identity >= opts$intraspecific_cutoff) %>%
  #remove last ;
  mutate(tax=gsub(";$", "", tax)) %>%
  # create source taxonomy; because the end of taxonomy has ";", so split will create 3 pieces
  tidyr::separate(tax, c("species", "accession", "accession2",
                                "desc", "taxonomy"),
                  sep = "\\|", remove = FALSE) %>%
  mutate(tmp=taxonomy) %>%
  #mutate(tmp=gsub("[dkpcofgst]__", "__", tmp)) %>%
  tidyr::separate(tmp, ranks.aodp, sep = ";", remove = TRUE) %>%
  as.data.frame()

########################
# aodp not included in final taxonomy
########################
suppressWarnings(
      aodp.filtered <- aodp[!(aodp$ASV %in% aodp.fil$ASV), ] %>%
                mutate(percent_identity=as.numeric(sub("%","", percent_identity))) %>%
                group_by(ASV) %>% # for each ASV choose only the best percent_identity
                arrange(dplyr::desc(percent_identity)) %>%
                as.data.frame()
  )
dim(aodp.fil)
dim(aodp.filtered)
dim(aodp)

# filtered not matched rows
dim(aodp.fil)
#1095   20
names(aodp.fil)
length(unique(aodp.fil$ASV)) # 113
head(aodp.fil)
# aodp.fil[aodp.fil$percent_identity <= 85, ]
# aodp.fil[aodp.fil$largest_cluster_matched !=1, ]

# generate species and strains
head(aodp.fil)
#Already split species and strain, so the following steps are not needed
#aodp.fil <- splitstackshape::cSplit(aodp.fil, "species", "|")
#head(aodp.fil)
## keep everything between ath and bth delimiter in R
#aodp.fil$species_1 <- sub_delimiter(3, 4,"_", aodp.fil$species_1)
#aodp.fil$species_1 <- gsub("^")
#gsub("^.*(Pucc.*)$", "\\1", aodp.fil$species_1)
#aodp.fil$strain <- aodp.fil$species
#aodp.fil$species <- sub_delimiter(0, 2, "_", aodp.fil$strain)
#head(aodp.fil)
#head(aodp.fil[, c("species", "strain")])

########################
# filter all refseq being "unidentified at a given rank
# This is important if you have a low-quality refDB, of Which
# many refseqs are assigned to p__unidentified or c__unidentified;
# it is not useful if a query sequence matched with such refseqs.
########################
blacklist <- c("unidentified", "unassignable", "unassigned", "Incertae_sedis")
print(paste0("remove unidentified records at ", opts$filter.unidentified.rank, " level"))
# remove unidentified at p__unidentified
blacklist.rk <- paste(paste(substr(opts$filter.unidentified.rank, 1, 1), "__", blacklist, sep=""),
                      collapse = "|")
blacklist.rk
aodp.fil <- aodp.fil %>%
  filter(!grepl(blacklist.rk, tax, ignore.case = TRUE))
head(aodp.fil)

########################
# select the best hit for each target (HTS seq)
# tmp <- aodp.fil[aodp.fil$ASV=="eae32af5b165d3f42ae058c6aedfb83a", ]
# find out for each ASV, how many unique percent_identity at each taoxnomic ranks
# this information will be used to get LCA (taxonomy concensus) for each ASV
########################

tmp_org <- aodp.fil %>%
  group_by(ASV, percent_identity) %>%
  #summarise_at(.vars = names(aodp.fil)[10],
  #              .funs = c(unique)) %>%
  summarise_at(.vars = names(aodp.fil)[which(names(aodp.fil) %in% ranks.aodp)],
               .funs = c(n_distinct)) %>%
  as.data.frame()
head(tmp_org)
#tmp_org[grepl("8760642efde623a2e051b38c3ca27c53", tmp_org$ASV), ]

# sanity check (at species level, query matches two species)
# tmp_org[tmp_org$species==2, ]
# ranks.aodp

########################
# decide which would be the LCA for each ASV
########################
tmp <- tmp_org
tmp$aodp.refseq <- rep("", nrow(tmp))
tmp$aodp.LCA <- rep("", nrow(tmp))
# tmp$aodp.LCA[104]
for (n in 1:nrow(tmp_org)) {
  print(tmp_org$ASV[n])
  sel <- max(which(tmp_org[n, ]==1))
  sel.1 <- names(tmp_org)[sel]
  sel.1
  tmp[n, sel.1]  <- unique(aodp.fil[aodp.fil$ASV==tmp_org$ASV[n], sel.1])
  print(paste0("LCA for ", tmp_org$ASV[n], ": ", gsub("", "", sel.1), " level: ",
               unique(aodp.fil[aodp.fil$ASV==tmp_org$ASV[n], sel.1])))

  tmp$aodp.LCA[n] <- unique(aodp.fil[aodp.fil$ASV==tmp_org$ASV[n], sel.1])
  # create the taxonomy for ranks ranks.aodp[1:rk.pos]
  rk.pos <- which(ranks.aodp==sel.1)
  rk.pos
  for (rkpos in 1:(rk.pos)) {
    rk <- ranks.aodp[rkpos]
    rk
    asign <- unique(aodp.fil[aodp.fil$ASV==tmp_org$ASV[n], paste0("", rk)])
    if ( length(asign) > 1) {
      asign <- paste(asign, collapse="|||")
    } else {
      asign = asign
    }
    tmp[n, rk]  <- asign
  }
  tmp[n, ]

  tmp$aodp.refseq[n] <- paste(unique(aodp.fil[aodp.fil$ASV==tmp_org$ASV[n], "tax"]), collapse =", ")

  # if sel.1=="strain", i.e. rk==rk_all
  # then aodp has identified the asv to the strain level, no need to continue
  rk_all <- length(ranks.aodp)
  rk_all
  if (rk.pos == rk_all) {
    next;
  } else if (rk.pos < rk_all) {
    # paste all possible classifications for the rest taxonomic ranks
    for (rkpos in (rk.pos+1):rk_all) {
      rk <- ranks.aodp[rkpos]
      rk
      if (rk == "strain") {
        rk_ <- substr(rk, 1, 2)
      } else {
        rk_ <- substr(rk, 1, 1)
      }
      rk
      rk_tax <- paste0(rk_, "__undefined:")
      tmp[n, rk]  <- paste0(rk_tax, paste(unique(aodp.fil[aodp.fil$ASV==tmp_org$ASV[n], paste0("", rk)]), collapse ="|"))
    }
  }
}
tmp[n,]
head(tmp)
# tmp[grepl("f35fd993aeeb5fe4bd03d023eccd4588", tmp$ASV), ]
#names(tmp)
#tmp[tmp$ASV=="8760642efde623a2e051b38c3ca27c53", ]
# define prefix for each rank of the taxonomy
#for (rk in ranks.aodp[which(ranks.aodp != "strain")]) {
#  tmp[, rk] <- paste0(substring(rk,1,1), "__", tmp[, rk])
#}
#tmp[, "strain"] <- paste0("st", "__", tmp[, "strain"])

########################
###create taxonomy at LCA (anything lower than LCA, will be NA, e.g. st__NA)
########################
head(tmp)
tmp$aodp.taxonomy <- do.call("paste", c(tmp[ranks.aodp], sep="; "))
head(tmp$aodp.taxonomy)
names(tmp)
head(tmp$ASV)
dim(tmp)
head(tmp)

names(tmp)[which(names(tmp) %in% ranks.aodp)] <- paste0("aodp.", names(tmp)[which(names(tmp) %in% ranks.aodp)])
names(tmp)
names(tmp)[which(names(tmp)=="percent_identity")] <- paste0("aodp.", names(tmp)[which(names(tmp)=="percent_identity")])

### return aodp.fil and aodp taxonomy (concensus)
out <- list()
out$aodp.fil = aodp.fil
out$aodp.filtered = aodp.filtered
out$aodp.tax = tmp
openxlsx::write.xlsx(out$aodp.fil, paste0(opts$output_basename, "_filter.xlsx"))
openxlsx::write.xlsx(out$aodp.filtered, paste0(opts$output_basename, "_filtered.xlsx"))
openxlsx::write.xlsx(out$aodp.tax, paste0(opts$output_basename, "_taxonomy.xlsx"))
write.table(out$aodp.fil, sep = "\t", col.names=NA, # by default no header for rownames
            paste0(opts$output_basename, "_filter.tab"), quote=FALSE)
write.table(out$aodp.filtered, sep = "\t", col.names=NA,
          paste0(opts$output_basename, "_filtered.tab"), quote=FALSE)
write.table(out$aodp.tax, sep = "\t", col.names=NA,
          paste0(opts$output_basename, "_taxonomy.tab"), quote=FALSE)
