package Primer;

##Object oriented data structure to hold and handle primer sequences and their derivatives
##Author: Devon Radford
##Created: September 12, 2019
##Modified: March 2, 2020

##use PrimerTree;

##my $debug=1;

sub new{
	my $class=shift;
	##my %args = @_;
	my $thisPrimer=shift;
	my $name=shift;
	##$args->{seq};
	$thisPrimer=~s/Y/[CTY]/g;
	$thisPrimer=~s/R/[GAR]/g;
	$thisPrimer=~s/S/[CGS]/g;
	$thisPrimer=~s/W/[ATW]/g;
	$thisPrimer=~s/K/[GTK]/g;
	$thisPrimer=~s/M/[ACM]/g;
	$thisPrimer=~s/B/[CGTB]/g;
	$thisPrimer=~s/V/[CGAV]/g;
	$thisPrimer=~s/D/[AGTD]/g;
	$thisPrimer=~s/H/[ACTH]/g;
	$thisPrimer=~s/[NI]/[ACTGIN]/g;
	
	my $compPrimer=reverse $thisPrimer;
	$compPrimer=~tr/ACGTYRSWKMBVDH[]/TGCARYWSMKVBHD][/;
	
	my @primerChar=split("",$thisPrimer);
	for($h=0;$h<=$#primerChar;$h++){
		if($primerChar[$h] eq "["){
       		$g=$h+1;
        	while($g<$#primerChar && $primerChar[$g] ne "]"){
       			$tmp="".$primerChar[$h].$primerChar[$g];
       			splice(@primerChar, $h, 2, $tmp);
			}
        	splice(@primerChar, $h, 2, $primerChar[$h]."]");
        }
    }
    my @primerQuarters=(join("",@primerChar[0..int($#primerChar/4)]),join("",@primerChar[(int($#primerChar/4)+1)..int($#primerChar/2)]),join("",@primerChar[(int($#primerChar/2)+1)..int(3*$#primerChar/4)]),join("",@primerChar[(int(3*$#primerChar/4)+1)..$#primerChar]));
    my @compPrimerChar=split("",$compPrimer);
	for($h=0;$h<=$#compPrimerChar;$h++){
		if($compPrimerChar[$h] eq "["){
       		$g=$h+1;
        	while($g<$#compPrimerChar && $compPrimerChar[$g] ne "]"){
       			$tmp="".$compPrimerChar[$h].$compPrimerChar[$g];
       			splice(@compPrimerChar, $h, 2, $tmp);
			}
        	splice(@compPrimerChar, $h, 2, $compPrimerChar[$h]."]");
        }
    }
    my @compPrimerQuarters=(join("",@compPrimerChar[0..int($#compPrimerChar/4)]),join("",@compPrimerChar[(int($#compPrimerChar/4)+1)..int($#compPrimerChar/2)]),join("",@compPrimerChar[(int($#compPrimerChar/2)+1)..int(3*$#compPrimerChar/4)]),join("",@compPrimerChar[(int(3*$#compPrimerChar/4)+1)..$#compPrimerChar]));
	
	my $self = bless {name => $name, seq => $thisPrimer, compSeq => $compPrimer, mismatch => \@{"$thisPrimer"}, compMismatch => \@{"$compPrimer"}, seqLength => $#primerChar, 
	quarters => \@primerQuarters, 
	compQuarters => \@compPrimerQuarters
	}, $class;
	print $self->{name},"\t",join("",@primerChar[0..int($#primerChar/4)]),"\t\"",$primerQuarters[0],"\"\t\"",$self->{quarters}[0],"\"\n" if($debug);
	##, seqTree => PrimerTree->new($thisPrimer,$compPrimer)
	##{join("",@primerChar[0..int($#primerChar/4)]),join("",@primerChar[int($#primerChar/4)..int($#primerChar/2)]),join("",@primerChar[int($#primerChar/2)..int(3*$#primerChar/4)]),join("",@primerChar[int(3*$#primerChar/4)..$#primerChar])}
	##{join("",@compPrimerChar[0..int($#compPrimerChar/4)]),join("",@compPrimerChar[int($#compPrimerChar/4)..int($#compPrimerChar/2)]),join("",@compPrimerChar[int($#compPrimerChar/2)..int(3*$#compPrimerChar/4)]),join("",@compPrimerChar[int(3*$#compPrimerChar/4)..$#compPrimerChar])}
	##print $self->{name},"\t",$self->{mismatch},"\t",$self->{compMismatch},"\n" if($debug);
	return $self;
}

sub getName{
	my ($self)= shift;
	return $self->{name};
}

sub setName{
	my ($self,$new_name) = @_;
	$self->{name} = $new_name;
}

sub getSeq{
	my ($self)= shift;
	return $self->{seq};
}

sub setSeq{
	my ($self,$new_seq) = @_;
	$self->{seq} = $new_seq;
	my $compPrimer = reverse $new_seq;
	$compPrimer=~tr/ACGTYRSWKMBVDH[]/TGCARYWSMKVBHD][/;
	$self->{compSeq}=$compPrimer;
}

sub getCompSeq{
	my ($self)= shift;
	return $self->{compSeq};
}

sub setCompSeq{
	my ($self,$new_seq) = @_;
	$self->{compSeq} = $new_seq;
	my $frwdPrimer = reverse $new_seq;
	$frwdPrimer=~tr/ACGTYRSWKMBVDH[]/TGCARYWSMKVBHD][/;
	$self->{seq}=$frwdPrimer;
}

sub getSeqQuarters{
	my ($self)=shift;
	return \@{$self->{quarters}};
}

sub getCompSeqQuarters{
	my ($self)=shift;
	return \@{$self->{compQuarters}};
}

sub getLength{
	my ($self)= shift;
	return $self->{seqLength};
}

sub getMismatchSeq{
	my ($self,$new_mismatch_num) = @_;
	if($new_mismatch_num <= 0){
		$new_mismatch_num=0;
		$self->{mismatch}[$new_mismatch_num][0]=("".$self->{seq});
	}elsif(!defined($self->{mismatch}[$new_mismatch_num])){
		$self->{mismatch}[$new_mismatch_num]=();
		if(!defined($self->{mismatch}[$new_mismatch_num-1])){
			$self->getMismatchSeq($new_mismatch_num-1);
		}
		my $variantCount=0;
		print "ITS9F mismatch[".($new_mismatch_num-1)."]:\t".$self->{mismatch}[$new_mismatch_num-1]."\n" if($self->{name} eq "ITS9F" && $debug);
		foreach $variant(@{$self->{mismatch}[$new_mismatch_num-1]}){
			if(ref($variant) eq 'ARRAY'){
			##	$variant=$variant->[0];
				print "variant should be string from array $variant length: ".$#{$variant}." [0]: ".$variant->[0]."\n" if($debug);
			}
			##print "ITS9F mismatch[".($new_mismatch_num-1)."]:".$variant."\n" if($self->{name} eq "ITS9F" && $debug);
			@primerChar=split("",$variant);
        	for($h=0;$h<=$#primerChar;$h++){
				if($primerChar[$h] eq "["){
       				$g=$h+1;
        			while($g<$#primerChar && $primerChar[$g] ne "]"){
       					$tmp="".$primerChar[$h].$primerChar[$g];
       					splice(@primerChar, $h, 2, $tmp);
	        		}
        			splice(@primerChar, $h, 2, $primerChar[$h]."]");
        		}
				if($primerChar[$h] eq "[ATCGNI]" || $primerChar[$h] eq "[ATCG]" || $primerChar[$h] eq "N" || $primerChar[$h] eq "I" || $primerChar[$h] eq "[ATCGN]"){
					next;
				}
        		##print "raw: @primerChar\n" if($self->{name} eq "ITS4R" && $debug);
    			my (@header,@footer)=((),());
    			if($h==0){
        			@footer=@primerChar[1..$#primerChar];
        			##print "@footer\n" if($self->{name} eq "ITS4R" && $debug);
        		}elsif($h==$#primerChar){
    				@header=@primerChar[0..($h-1)];
    			}else{
		        	@header=@primerChar[0..($h-1)];
		    		@footer=@primerChar[($h+1)..$#primerChar];
		        }
		        $self->{mismatch}[$new_mismatch_num][$variantCount]=join("",@header,"[ATCGNI]",@footer);
	        	if($debug==1 && $self->{name} eq "ITS9F"){
	    		##	print "$new_mismatch_num , 0..".($h-1).",".($h+1)."..".$#primerChar.": ".$self->{mismatch}[$new_mismatch_num][$variantCount]."\n";
	    		}
        		$variantCount++;
	        }
	    }
	}
	print "m: $new_mismatch_num\t".$self->{mismatch}[$new_mismatch_num]."\n" if($self->{name} eq "ITS9F" && $debug);
	return @{$self->{mismatch}[$new_mismatch_num]};
}

sub getCompMismatchSeq{
	my ($self,$new_mismatch_num) = @_;
	if($new_mismatch_num <= 0){
		$new_mismatch_num=0;
		$self->{compMismatch}[$new_mismatch_num][0]=("".$self->{compSeq});
		##print "ITS9F compMismatch[0]: ".$self->{compMismatch}[$new_mismatch_num]."\n" if($self->{name} eq "ITS9F" && $debug);
	}elsif(!defined($self->{compMismatch}[$new_mismatch_num])){
		$self->{compMismatch}[$new_mismatch_num]=();
		if(!defined($self->{compMismatch}[$new_mismatch_num-1])){
			$self->getCompMismatchSeq($new_mismatch_num-1);
		}
		my $variantCount=0;
		print "ITS9F comp-mismatch[".($new_mismatch_num-1)."]:\t".$self->{compMismatch}[$new_mismatch_num-1]."\n" if($self->{name} eq "ITS9F" && $debug);
		foreach $variant(@{$self->{compMismatch}[$new_mismatch_num-1]}){
			if(ref($variant) eq 'ARRAY'){
				##	$variant=$variant->[0];
				print (($new_mismatch_num-1).": comp variant should be string from array $variant length: ".$#{$variant}." [0]: ".$variant->[0]."\n") if($debug);
			}
			@primerChar=split("",$variant);
		    for($h=0;$h<=$#primerChar;$h++){
    			if($primerChar[$h] eq "["){
        			$g=$h+1;
        			while($g<$#primerChar && $primerChar[$g] ne "]"){
    					$tmp="".$primerChar[$h].$primerChar[$g];
        				splice(@primerChar, $h, 2, $tmp);
      				}
        			splice(@primerChar, $h, 2, $primerChar[$h]."]");
        		}
				if($primerChar[$h] eq "[ATCGNI]" || $primerChar[$h] eq "[ATCG]" || $primerChar[$h] eq "N" || $primerChar[$h] eq "I" || $primerChar[$h] eq "[ATCGN]"){
					next;
				}
	        	my (@header,@footer)=((),());
        		if($h==0){
    				@footer=@primerChar[1..$#primerChar];
        		}elsif($h==$#primerChar){
        			@header=@primerChar[0..($h-1)];
        		}else{
		       		@header=@primerChar[0..($h-1)];
		        	@footer=@primerChar[($h+1)..$#primerChar];
			    }
				$self->{compMismatch}[$new_mismatch_num][$variantCount]=join("",@header,"[ATCGNI]",@footer);
				$variantCount++;
			}
		}
	}
	print "cm: $new_mismatch_num\t".$self->{compMismatch}[$new_mismatch_num]."\n" if($self->{name} eq "ITS9F" && $debug);
	return @{$self->{compMismatch}[$new_mismatch_num]};
}

1;