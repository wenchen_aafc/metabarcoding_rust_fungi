# README #

## Introduction ##
This repository documents the source codes of the bioinformatics tools and contains reference databases developed for manuscript "Chen, Wen*, Radford, Devon & Hambleton, Sarah (2021) _Towards improved recovery efficiency and identification accuracy of rust fungal pathogens in environmental samples using metabarcoding approach_.

This README describes how to run the scripts in Linux and/or OSX operation systems.

## Steps ##
### Clone this repository

> git clone https://Wen_Chen@bitbucket.org/wenchen_aafc/metabarcoding_rust_fungi.git

> cd metabarcoding_rust_fungi

### Download reference databases from "Downloads" and save to "example" folder: ###
* refDB.tar.gz contains two refDBs used by IsPRIMER: F-refDB.fasta & R-refDB.fasta
* CR-ITS2-refDB.fasta: the in-house Puccinia ITS2 refDB;
* CR-ITS2-refDB.align.fasta: the alignment of CR-ITS2-refDB.fasta using MAFFT (v7.453) 
Primer lists: primerList.txt, primerF.txt (forward primers), primerR.txt (reverse primers);
* Pucciniales_asv.fasta: the representative sequences of all Pucciniales ASVs from this study.

### For in-silico evaluation of primer specificity and coverage, run IsPRIMER in Linux or OSX as: ###

> ./IsPRIMER.pl --p example/primerR.txt --r example/R-refDB.fasta --o output/primerR.vs.R-refDB

* To merge all IsPIMER outputs and to produce "output/R-refDB.isprimer.merge.1.isprime.out"

> ./IsPRIMER_merge.pl --p output/ --n primerR.vs.R-refDB.1  --o output/merge.primerR.vs.R-refDB.1 --t example/R-refDB.fasta

### To summarize primer specificity and coverage of each primer based on IsPRIMER outputs ###
> ./IsPRIMER_summary.pl --t output/merge.primerR.vs.R-refDB.1.csv --rk genus --o output/

### If you run above scripts in Mac OSX terminal, you may need to replace "^M" carriage return control character with Unix line endings, e.g. ###

> `# tr '\r' '\n' < input_file > output_file`

### To calculate inter- and intra-specific variation of a sequence alignment, use run_SeqVar.R in Linux or OSX as:###
> Rscript --vanilla run_SeqVar.R -a example/CR-ITS2-refDB.align.fasta

* The input alignment must be in FASTA format
* FASTA header should be in the format of General FASTA Release Format of the UNITE ITS project, i.e.
  Puccinia_coronati.brevispora|HM131236|DAOM235159|CR|k__Fungi;p__Basidiomycota;c__Pucciniomycetes;o__Pucciniales;f__Pucciniaceae;g__Puccinia;s__Puccinia_coronati.brevispora;st__Puccinia_coronati.brevispora_Rhamnus_CAN_DAOM235159_2005
* The script installs all required R packages if having not been installed.

### To use AODP to classify sequences, follow the steps below in Linux system ONLY: ###
* Download aodp-2.5.0.1.tar.gz from: https://bitbucket.org/wenchen_aafc/aodp_v2.0_release/src/master/
* unpack the compressed file

> tar xvzf aodp-2.5.0.1.tar.gz`

* switch to the aodp folder

> cd aodp-2.5.0.1

* Install AODP by following the instruction in "INSTALL" file.
* To runAODP, prepare a high-quality refDB in UNITE General Release format, here we use CR-ITS2-refDB.fasta that you have downloaded and saved to the "example" folder. We will use "Pucciniales_asv.fasta" as the query sequence FASTA file, which includes the representative sequences of all Pucciniales ASVs from the current study.

> time aodp --threads=8 --match=Pucciniales_asv.fasta --oligo-size=16 --match-output=Pucciniales_asv.vs.CRITS2refDB.16.aodp --max-homolo=0 example/CR-ITS2-refDB.fasta

### Contribution ###
* run_SeqVar.R was written by Dr. W. Chen @ Ottawa RDC, AAFC, Canada
* IsPRIMER.pl was written by Dr. D. Radford @ Ottawa RDC, AAFC, Canada
* CR-ITS2-refDB: the original database was developed by Dr. S. Hambleton @ Ottawa RDC, AAFC, Canada; Dr. W. Chen reformatted the DB, made the alignment and reconstructed the phylogenetic tree.
* F-refDB was compiled by Dr. W. Chen. It contains 636,344 sequences with full length 5.8S. It has 6,475 Pucciniales sequences.
* R-refDB was compiled by Dr. W. Chen. It contains 166,140 sequences with ITS2 + partial 28S. It has 2,566 Pucciniales sequences.
* Other guidelines

### Who do I talk to? ###
* Dr. W. Chen: wen.chen@canada.ca
* Dr. S. Hambleton: sarah.hambleton@canada.ca
* Dr. D. Radford: devon.radford@canada.ca

### Copyright ###
Copyright HER MAJESTY THE QUEEN IN RIGHT OF CANADA (2020-2021).
All content in this repository is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License (version 3) for more detaila. You should have received a copy of the GNU General Public License (version 3) along with this program. If not, see http://www.gnu.org/licenses/.

### Acknowledgement ###
This study was supported by Agriculture and Agri-Food Canada funded A-base projects J-002216, J-002272, and J-002366. Development of previously unpublished reference DNA sequences was supported in part by funding from the Genomics Research and Development Initiative (GRDI-QIS, Project ID 2679) of the Government of Canada (https://grdi.canada.ca/en). The 2009 spore trap network in Canada was supported by Agriculture and Agri-Food Canada, the Grain Farmers of Ontario and the Ontario Ministry of Agriculture, Food and Rural Affairs. We thank Dr. Shannon Xuechan Shan (Plant Disease Clinic and Immunochemistry Laboratory, Laboratory Services Division, University of Guelph) for sharing the primer ITS4var_S. We thank Dr. Kitty Cheung and Quinn Eggertson for technical assistance with DNA processing and sequencing, and Julie Carey for preparing the GenBank submissions.
