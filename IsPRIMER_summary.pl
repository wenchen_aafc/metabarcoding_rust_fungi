#!/usr/bin/env perl
#use strict;
#use warnings;
use FindBin;
use lib "$FindBin::Bin/Modules";
use File::Find;
use File::Spec;
use File::Basename;
use List::MoreUtils qw(first_index);
use List::MoreUtils qw(firstidx);
use Text::CSV::Hashify;

# # summarise at species level
my $start=time();

my @time_start = localtime();
# generates YYYYMMDDhhmmss;
my $timestring1 = sprintf("%04d%02d%02d",
                        $time_start[5]+1900, $time_start[4]+1, $time_start[3]);
my $timestring = sprintf("%04d-%02d-%02d %02d:%02d:%02d",
				 $time_start[5]+1900, $time_start[4]+1, $time_start[3],
				 $time_start[2],      $time_start[1],   $time_start[0]);

print "\nProcess started at ", $timestring, "\n";


# usage: ./IsPRIMER_summary --t merged_IsPRIMER_outputs.csv --rk phylum --o output_folder
# make sure input IsPRIMER merged file should be in CSV format.

my $taxonomy;
my $low_tx;
my $output;

my $lastFlag="";
foreach $arg(@ARGV){
	if($lastFlag eq "--o"){
		$output="$arg";
	}elsif($lastFlag eq "--rk"){
		$low_tx="$arg";
	}elsif($lastFlag eq "--t"){
		$taxonomy="$arg";
  }elsif($lastFlag eq "--h"){ ##help
		print "IsPRIMER_summary.pl \nAuthors: Devon Radford, Wen Chen\n";
		print "Purpose: summarize IsPRIMER.pl outputs at give\n";
		print "Command options:\n";
		print "\t--h\thelp\n";
		print "\t--t\tmerged IsPRIMER.pl outputs in CSV format, required\n";
		print "\t--rk\tthe rank to summarize from (kingdom, phylum,class, order, family, genus, species) (type:
                        Symbol, default: order) (Required)\n";
    print "\t--o\tthe output folder (required)\n";
		exit;
	}
	$lastFlag="$arg";
}
if($lastFlag eq "--h"){ ##help
    print "IsPRIMER_summary.pl \nAuthors: Devon Radford, Wen Chen\n";
    print "Purpose: summarize IsPRIMER.pl outputs at give\n";
    print "Command options:\n";
    print "\t--h\thelp\n";
    print "\t--t\tmerged IsPRIMER.pl outputs in CSV format, required\n";
    print "\t--rk\tthe rank to summarize from (kingdom, phylum,class, order, family, genus, species) (type:
                        Symbol, default: order) (Required)\n";
    print "\t--o\tthe output folder (required)\n";
	exit;
}

#print join(", ", $output, $taxonomy, $low_tx, $high_tx), "\n";

my @suffix=(".csv", ".out", ".txt");
my $outname = basename($taxonomy, @suffix);
#print "$output/$outname.$low_tx.$high_tx.all_species_at_rank.summary\n";


# summarize at which taxonomic resolution:
my @ranks=("kingdom", "phylum", "class", "order", "family", "genus", "species");
my $tx_low= firstidx { $_ eq $low_tx } @ranks;

# Kingdom=0, phylum=1, class=2, order=3, family=4, genus=5, species=6";

###################
# store information in hash
###################
# Input must be in CSV format

# store information in $obj (hash reference)
$obj = Text::CSV::Hashify->new( {
    file        => $taxonomy,
    format      => 'hoh', # hash of hashes, which is default
    key         => 'ACC',  # needed except when format is 'aoh'
    #max_rows    => 20,   # number of records to read; defaults to all
    #... # other key-value pairs as appropriate from Text::CSV;
} );

$keys_ref = $obj->keys; # value is array reference
#print "@{$keys_ref}\n";

print "\nSummarize ", scalar @{$keys_ref}, " accessions at $low_tx", "\n";


$fields_ref = $obj->fields; # value is array reference;
#print "@$fields_ref\n";
my @mismatch_fields=grep { /_mismatchNum/}  @$fields_ref;
#print join(";", @mismatch_fields), "\n";

#print "\n@mismatch_fields\n";
my @primers;
foreach my $mf (@mismatch_fields) {
	my $pr = $mf;
	$pr=~ s/_mismatchNum//g;
	push @primers, $pr unless grep { $_ eq $pr } @primers;
}
print "\n", scalar @primers, " primers to be summarized which including:  ", join(" ", @primers), "\n";
#print join(";", @mismatch_fields), "\n";


##################
# parsing $obj
##################
my $h_taxonomy={}; #store parent nodes for each taxon at each rank.
foreach my $acc (@{$keys_ref}) {
	#print $acc, "\t";
	$record_ref = $obj->record($acc); # value is hash reference
	#print "$acc0\t$record_ref->{$acc0}\n";
	#foreach my $r (keys %$record_ref) {
	my $rk = $ranks[$tx_low];
	#print $rk, "\t";
	my $tx=$record_ref->{$rk};
	my $species=$record_ref->{"name"};

	my @rk_parent;
	if ($tx_low==0) {
		@rk_parent = "";
	} elsif ($tx_low==1) {
		@rk_parent = $ranks[0];
	} else {
		@rk_parent = @ranks[0..$tx_low];
	}
	#print "@rk_parent\t";

	$h_taxonomy->{$tx}->{"rank"}=$rk;
	my @tx_parent;
	foreach my $rk_p ( @rk_parent) {
		push @tx_parent, $record_ref->{$rk_p};
	}
	$h_taxonomy->{$tx}->{"parent_nodes"} = join(";", @tx_parent);

  foreach my $pr (@primers) {
     #my $pr="ITS4rust";
     my $pr_mm = $pr."_mismatchNum";
     # save all species for a taxon as array
     push @{$h_taxonomy->{$tx}->{$species}->{$pr}->{"ACC"}}, $acc;

     if ( $record_ref->{$pr_mm} eq 0) {
       # perfectly matched $acc
       #print $species,"|MM0:", $acc, "\t";
       push @{$h_taxonomy->{$tx}->{$species}->{$pr}->{"MM0"}}, $acc;
     } elsif ( $record_ref->{$pr_mm} eq 1) {
       # 1 MM $acc
       #print $species,"|MM1:", $acc, "\n";
       push @{$h_taxonomy->{$tx}->{$species}->{$pr}->{"MM1"}}, $acc;
     } elsif ( $record_ref->{$pr_mm} eq 2) {
       # 2 MM $acc
       push @{$h_taxonomy->{$tx}->{$species}->{$pr}->{"MM2"}}, $acc;
     } elsif ( $record_ref->{$pr_mm} eq "-") {
       # > 2 MM $acc
       push @{$h_taxonomy->{$tx}->{$species}->{$pr}->{"MMgt2"}}, $acc;
     }
  }
}

# write reports:
# number of sequences matched for each species of each taxa at a give rank;
open (OUT1, ">$output/$outname.CoverOfSeq.for.EachSP.at.$low_tx.summary");
# number of species matched for each taxa at a give rank;
# note, here a long as 1 seq of a species matched with a primer, then
# the species is considered matched with the primer, even if there other
# specimens in the species didn't match the given primer
# priority: MM0 -> MM1 -> MM2 -> MMgt2
open (OUT2, ">$output/$outname.CoverOfSP.for.Each.$low_tx.summary");
# number of sequences matched for each taxa at a give rank;
open (OUT3, ">$output/$outname.CoverOfSeq.for.Each.$low_tx.summary");

# create output header

my @header1= ("rank", "name", "parent_nodes", "total_sp.num",
              "sp.name", "total_sp.seq", "primer", "MM0", "MM1", "MM2", "MMgt2");
my @header2= ("rank", "name", "parent_nodes", "total_seq", "total_sp.num",
              "primer", "MM0", "MM1", "MM2", "MMgt2");

print OUT1 join(",", @header1), "\n";
print OUT2 join(",", @header2), "\n";
print OUT3 join(",", @header2), "\n";

foreach my $tx (sort keys %$h_taxonomy){
  #print $tx, "\n";
  my $num_sp= keys %{$h_taxonomy->{$tx}};
  $num_sp = $num_sp - 2;

  # remove $tx eqs to "parent_nodes" & "rank"
  #print $num_sp - 2, " species in ", $tx, "\t";

  foreach my $pr (@primers) {
    #my $pr = "ITS4rust";
    my $num_sp_MM0;
    my $num_sp_MM1;
    my $num_sp_MM2;
    my $num_sp_MMgt2;
    my $num_seq_tx;

    my $num_MM0_tx;
    my $num_MM1_tx;
    my $num_MM2_tx;
    my $num_MMgt2_tx;

    foreach my $sp (sort keys %{$h_taxonomy->{$tx}}) {
      my $num_MM0;
      my $num_MM1;
      my $num_MM2;
      my $num_MMgt2;
      my $num_seq;
      if ( $sp eq "parent_nodes") {
        next;
      } elsif ($sp eq "rank") {
        next;
      } elsif ( exists $h_taxonomy->{$tx}->{$sp}->{$pr} ) {
              #$sp = "Puccinia_graminis_f._sp._avenae";
        #print $sp, "\t";
        $num_seq= (scalar @{$h_taxonomy->{$tx}->{$sp}->{$pr}->{"ACC"}});
        $num_seq_tx = $num_seq_tx + $num_seq;
        #print $sp, ": ", scalar @num_seq, " seqs\t";

        # coverage for sequences of each species,
        # e.g. Puccinia_graminis_f._sp._avenae
        # in a given tax (e.g. order of Pucciniales)
        $num_MM0= (scalar @{$h_taxonomy->{$tx}->{$sp}->{$pr}->{"MM0"}});
        $num_MM0_tx=$num_MM0_tx + $num_MM0;
        $num_MM1= (scalar@{$h_taxonomy->{$tx}->{$sp}->{$pr}->{"MM1"}});
        $num_MM1_tx=$num_MM1_tx + $num_MM1;
        $num_MM2= (scalar@{$h_taxonomy->{$tx}->{$sp}->{$pr}->{"MM2"}});
        $num_MM2_tx=$num_MM2_tx + $num_MM2;
        $num_MMgt2= (scalar@{$h_taxonomy->{$tx}->{$sp}->{$pr}->{"MMgt2"}});
        $num_MMgt2_tx=$num_MMgt2_tx+$num_MMgt2;

        # coverage for species in a given tax (e.g. order of Pucciniales)
        if ($num_MM0 gt 0) {
          #print scalar @num_MM0, "\t";
          $num_sp_MM0++;
        } elsif ($num_MM1 gt 0) {
          $num_sp_MM1++;
        } elsif ($num_MM2 gt 0) {
          $num_sp_MM2++;
        } elsif ($num_MMgt2 gt 0) {
          $num_sp_MMgt2++;
        }

        print OUT1 join(",", $h_taxonomy->{$tx}->{"rank"}, $tx,
          $h_taxonomy->{$tx}->{"parent_nodes"}, $num_sp, $sp, $num_seq,
          $pr, $num_MM0, $num_MM1, $num_MM2, $num_MMgt2), "\n";
      }
    }
    print OUT2 join(",", $h_taxonomy->{$tx}->{"rank"}, $tx,
      $h_taxonomy->{$tx}->{"parent_nodes"}, $num_seq_tx, $num_sp, $pr,
      $num_sp_MM0, $num_sp_MM1, $num_sp_MM2, $num_sp_MMgt2), "\n";

    print OUT3 join(",", $h_taxonomy->{$tx}->{"rank"}, $tx,
        $h_taxonomy->{$tx}->{"parent_nodes"}, $num_seq_tx, $num_sp, $pr,
        $num_MM0_tx, $num_MM1_tx, $num_MM2_tx, $num_MMgt2_tx), "\n";

  }
}

close OUT1;
close OUT2;
close OUT3;

print "summaries are saved to:\n";
print "$output/$outname.CoverOfSeq.for.EachSP.at.$low_tx.summary", "\n";
print "$output/$outname.CoverOfSP.for.Each.$low_tx.summary", "\n";
print "$output/$outname.CoverOfSeq.for.Each.$low_tx.summary", "\n";
