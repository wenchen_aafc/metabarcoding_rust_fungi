#!/usr/bin/env perl


#use strict;
#use warnings;
#use Bio::SeqIO;
use FindBin;
use lib "$FindBin::Bin/Modules";
use File::Find;
use File::Spec;
use File::Basename;


# usage: perl ~/Documents/ubuntu_64bit_shared/Bitbucket_repos/isprime_wen/isPRIME_merge.pl . Fungi_28S_rettype_is_fasta_gb_20200305.clean.parse.ITS2-28S.fasta.1 Fungi_28S_rettype_is_fasta_gb_20200305.clean.parse.ITS2-28S.fasta

my $start=time();

my @time_start = localtime();
# generates YYYYMMDDhhmmss;
my $timestring1 = sprintf("%04d%02d%02d",
                        $time_start[5]+1900, $time_start[4]+1, $time_start[3]);
my $timestring = sprintf("%04d-%02d-%02d %02d:%02d:%02d",
				 $time_start[5]+1900, $time_start[4]+1, $time_start[3],
				 $time_start[2],      $time_start[1],   $time_start[0]);

print "\nProcess started at ", $timestring, "\n";

# merge isPRIME outputs for each tested primer

my $path;
my $file_pattern;
my $outname;
my $input_tax;

my $lastFlag="";
foreach $arg(@ARGV){
	if($lastFlag eq "--o"){
		if($testFlag){
			$outname="$arg".".$timestring".".tab";
		}else{
			$outname="$arg";
		}
	}elsif($lastFlag eq "--p"){
		$path="$arg";
	}elsif($lastFlag eq "--n"){
		$file_pattern="$arg";
	}elsif($lastFlag eq "--o"){
		$outname=$arg;
	}elsif($lastFlag eq "--t"){
		$input_tax=$arg;
	}elsif($lastFlag eq "--h"){ ##help
		print "isPRIMER_merge.pl \nAuthors: Devon Radford, Wen Chen\n";
		print "Purpose: merge IsPRIMER.pl output tables\n";
		print "Command options:\n";
		print "\t--h\thelp\n";
		print "\t--p\tPath to IsPRIMER.pl outputs to be merged, required\n";
		print "\t--n\tBasename of IsPRIMER.pl outputs to be merged in the Path (required)\n";
    print "\t--o\tBasename of the merged table (required)\n";
		print "\t--t\tThe refDB in UNITE general release format used by IsPRIMER.pl (required)\n";
		exit;
	}
	$lastFlag="$arg";
}
if($lastFlag eq "--h"){ ##help
    print "isPRIMER_merge.pl \nAuthors: Devon Radford, Wen Chen\n";
    print "Purpose: merge IsPRIMER.pl output tables\n";
    print "Command options:\n";
    print "\t--h\thelp\n";
		print "\t--p\tPath to IsPRIMER.pl outputs to be merged, required\n";
		print "\t--n\tBasename of IsPRIMER.pl outputs to be merged in the Path (required)\n";
    print "\t--o\tBasename of the merged table (required)\n";
		print "\t--t\tThe refDB in UNITE general release format used by IsPRIMER.pl (required)\n";
	exit;
}


opendir(DIR, "$path");
my @files = grep(/$file_pattern/,readdir(DIR));
closedir(DIR);

my $h_merge={};
my $h_same={};
my @primers;
my @header=("ACC", "name", "seq_length", "taxonomy", "kingdom", "phylum", "class", "order", "family", "genus", "species");

open (OUT, ">$outname.csv");
open (ERROR, ">$outname.error");
foreach $file (@files) {
	print "$file\n";
   	# Match a dot, followed by any number of non-dots until the
   	# end of the line.
   	my ($ext) = $file =~ /(\.[^.]+)$/;
   	push @primers, $ext;
   	open(IN, "<$path/$file");
   	while (my $line=<IN>) {
		chomp $line;
	  if ($line=~ m/^#/) {
			#print OUT $line, "\n";
		} else {
			my ($GI, $ACC, $name, $seq_length, $mismatchNum, $strand, $match) = split /\t/, $line;
			if ($line=~ m/^GI\t/) {
				push @header, ($mismatchNum, $strand, $match);
			} else {
				$h_merge->{$ext}->{$ACC}= join(",", $mismatchNum, $strand, $match);
				if (!exists $h_same->{$ACC}) {
					$h_same->{$ACC}->{"name"}=$name;
					$h_same->{$ACC}->{"seq_length"}=$seq_length;
				} elsif ( exists $h_same->{$ACC}) {
					if ( join(",", $ACC, $name, $seq_length) eq join(",", $ACC, $h_same->{$ACC}->{"name"}, $h_same->{$ACC}->{"seq_length"}) ) {
						next;
					} else {
						print ERROR "ERROR: missmatching information about $ACC\n";

					}

				}
			}
		}
	}

      close IN;
}

############################
## obtain taxonomy:
############################
open (IN, "<$input_tax");
while ( my $ln=<IN> ) {
	chomp $ln;
	if ( $ln=~ m/^>/) {
		#print $ln, "\n";
		my @des = split /\|/, $ln;
		my $n = $des[0];
		$n=~ s/^>//g;
		my $ac = $des[1];
		my $taxonomy = $des[$#des];
		#print $ac, "\t", $taxonomy, "\n";
		if (exists $h_same->{$ac}) {
			$h_same->{$ac}->{"name"}=$n;
			$h_same->{$ac}->{"taxonomy"}=$taxonomy;

			my ($kingdom, $phylum, $class, $order, $family, $genus, $species) = split /[;\s]+/, $taxonomy;
			$h_same->{$ac}->{"kingdom"}=$kingdom;
			$h_same->{$ac}->{"phylum"}=$phylum;
			$h_same->{$ac}->{"class"}=$class;
			$h_same->{$ac}->{"order"}=$order;
			$h_same->{$ac}->{"family"}=$family;
			$h_same->{$ac}->{"genus"}=$genus;
			$h_same->{$ac}->{"species"}=$species;

			#print join("\t", $ac, $h_same->{$ac}->{"head"}, $h_same->{$ac}->{"taxonomy"}), "\n";
		} else {
			print ERROR "ERROR: missing taxonomy about $ac\n";
		}
	}
}
close IN;
close ERROR;

# merge all tables
print join(",", @header), "\n";
print OUT join(",", @header), "\n";

my $count=0;
for my $acc (keys %$h_same){
	print OUT join(",", $acc, $h_same->{$acc}->{"name"}, $h_same->{$acc}->{"seq_length"}, $h_same->{$acc}->{"taxonomy"}), ",";
	print OUT join(",", $h_same->{$acc}->{"kingdom"}, $h_same->{$acc}->{"phylum"}, $h_same->{$acc}->{"class"}), ",";
	print OUT join(",", $h_same->{$acc}->{"order"}, $h_same->{$acc}->{"family"}, $h_same->{$acc}->{"genus"}), ",";
	print OUT $h_same->{$acc}->{"species"}, ",";
	#print $acc, $h_same->{$acc}, "\t";
	foreach my $primer (@primers) {
		#print "\n", $primer, ": ";
		if (exists $h_merge->{$primer}->{$acc}) {
			print OUT $h_merge->{$primer}->{$acc}, ",";
		}
	}
	print OUT "\n";
	$count++;
	if ($count % 10000 == 0) {
		print $count, "...";
	}
}
print $count, " accessions has been merged and saved to $outname.csv!\n\n";
close OUT;
